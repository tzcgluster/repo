Name:		UCC
Version:	2015.12.7
Release:	1%{?dist}
Summary:	Unified Code Counter (UCC)

Group:		Development/Tools
License:	http://csse.usc.edu/csse/affiliate/private/license.txt
URL:		http://csse.usc.edu/ucc_new/wordpress/
Source0:	%{name}-%{version}.tar.gz

BuildRequires:	boost-thread boost-random boost-atomic boost-date-time boost-iostreams boost-filesystem boost-locale boost-math boost-signals boost boost-regex boost-graph boost-test boost-python boost-serialization boost-system boost-wave boost-program-options boost-chrono boost-timer boost-context gcc-c++
Requires:	boost-thread boost-regex boost-system

%description
The Unified Code Counter (UCC) is a comprehensive software lines of code counter produced by the USC Center for Systems and Software Engineering.

%prep
%autosetup


%build
make %{?_smp_mflags}


%install
make install prefix=%{buildroot}/usr/local


%files
%defattr(-,root,root)
/usr/local/bin/UCC

%changelog

