#!/bin/bash

mkdir -p ~/rpmbuild/SOURCES
mkdir -p ~/rpmbuild/SPECS
git archive --remote git@bitbucket.org:tzcgluster/ucc.git -o ~/rpmbuild/SOURCES/UCC-2015.12.7.tar.gz --prefix=UCC-2015.12.7/ master
cp SPECS/ucc.spec ~/rpmbuild/SPECS/
pushd ~/rpmbuild
rpmbuild -v -ba --sign --clean SPECS/ucc.spec
mkdir -p /var/www/html/dknowrepo/x86_64/
/bin/cp ~/rpmbuild/RPMS/x86_64/*.rpm /var/www/html/dknowrepo/x86_64/
createrepo --database /var/www/html/dknowrepo/x86_64/
/bin/cp RPM-GPG-KEY-dknow dknow.repo /var/www/html/dknowrepo/
chmod -R a+r /var/www/html/dknowrepo/
chmod a+x /var/www/html/dknowrepo/x86_64/ /var/www/html/dknowrepo/x86_64/repodata/
chown -R nginx:nginx /var/www/html/dknowrepo/
pushd /var/www/html/dknowrepo
find -type f -exec sha256sum {} \; >sha256sums.txt
popd
