import hashlib
import requests
import sys

BASE_URL = "https://devapi.dknow.eu/dknowrepo/"

with open('/var/www/html/dknowrepo/sha256sums.txt') as f:
	for line in f:
		line = line.strip()
		if not line:
			continue
		checksum, fname = line.split('  ')
		fname = fname[2:]
		if fname == 'sha256sums.txt':
			continue
		url = BASE_URL+fname
		h = hashlib.sha256()
		sys.stderr.write("%s\n" % url)
		r = requests.get(url, stream=True)
		for block in r.iter_content():
			h.update(block)
		digest = h.hexdigest()
		if digest <> checksum:
			print h.hexdigest(), checksum, fname
		
